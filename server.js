const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const path = require('path');

const app = express();
app.use(bodyParser.json());
app.use(express.static('public')); // Serve static files from 'public' directory

// Function to read data from JSON file
const readData = () => {
  const jsonData = fs.readFileSync('containers.json');
  return JSON.parse(jsonData);
};

// API endpoint to fetch container data
app.get('/api/container/:id', (req, res) => {
  const { id } = req.params;
  const containers = readData();
  const container = containers[id];

  if (container) {
    res.json(container);
  } else {
    res.status(404).send('Container not found');
  }
});

// Serve index.html on the root path
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, '/public/index.html'));
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Server running on port ${PORT}`));

